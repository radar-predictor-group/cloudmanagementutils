from google.cloud import storage
from config import BUCKET_NAME
import os


class StorageHandler:

    def __init__(self, basepath, logger):
        self._basepath = basepath
        self._logger = logger
        self._client = storage.Client()
        self._bucket = self._client.get_bucket(BUCKET_NAME)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._client = None
        self._bucket = None

    def upload(self, local_filename, cloud_filename):
        blob = self.__get_blob(cloud_filename)
        blob.upload_from_filename(filename=local_filename, content_type=self.__get_content_type(local_filename))

    def download(self, cloud_full_path, local_dir):
        (cloud_path, cloud_filename) = os.path.split(cloud_full_path)
        full_local_dir_path = os.path.join(local_dir, cloud_path)
        os.makedirs(full_local_dir_path)
        blob = self.__get_blob(cloud_full_path)
        full_local_file_path = os.path.join(full_local_dir_path, cloud_filename)
        blob.download_to_filename(full_local_file_path)

    def delete(self, cloud_filename):
        blob = self.__get_blob(cloud_filename)
        blob.delete()

    def delete_dir(self, base_cloud_path=''):
        blobs = self.__get_all_blobs(base_cloud_path=base_cloud_path)
        for blob in blobs:
            blob.delete()

    def exists(self, cloud_filename):
        blob_filename = self.__get_full_path(cloud_filename)
        return storage.Blob(bucket=self._bucket, name=blob_filename).exists(self._client)

    def count(self, base_cloud_path=''):
        """
        WARNING! This function would be very on a medium to large bucket and generate a lot of traffic!
        As such, it doesn't seem very useful, but for test purposes with a small number of files it should
        be fine.
        :return: The number of blobs in the bucket
        """
        blobs = self.__get_all_blobs(base_cloud_path=base_cloud_path)
        return sum(1 for _ in blobs)

    def __get_blob(self, cloud_filename):
        blob_filename = self.__get_full_path(cloud_filename)
        return self._bucket.blob(blob_filename)

    def __get_all_blobs(self, base_cloud_path):
        full_cloud_path = self.__get_full_path(base_cloud_path)
        blobs = self._bucket.list_blobs(prefix=full_cloud_path)
        return blobs

    def __get_full_path(self, filename):
        return os.path.join(self._basepath, filename)

    def __get_content_type(self, local_filename):
        import magic
        mime = magic.Magic(mime=True)
        return mime.from_file(local_filename)
