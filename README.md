# CloudManagementUtils

A collection of utilities for managing tasks on GCP, such as downloading files to the local machine and deleting them off the cloud.