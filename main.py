import argparse

def get_parsed_args():
    parser = argparse.ArgumentParser(
        description='Utils for managing Google Cloud Platform features')
    # parser.add_argument('--', '-e', default=1, type=int, help='the number of epochs to train for')
    # parser.add_argument('--window_secs', '-w', default=30, type=int, help='flow data window size in seconds')
    # parser.add_argument('--overlap', '-o', default=5, type=int, help='flow data window size in seconds')
    # parser.add_argument('--find_own_fft', '-f', action='store_true',
    #                     help='If present, the model will be augmented to be capable of learning the equivalent of the FFT first')
    # parser.add_argument('--cutoff_frequency', '-c', default=1, type=float, help='The cutoff frequency to use when generating FFTs')
    # parser.add_argument('--use_convolution', '-v', action='store_true',
    #                     help='If present, the model will use 1d convolution')
    # parser.add_argument('--go_deep', '-g', action='store_true', help='If present, the model will use more layers')
    # parser.add_argument('--prebuilt_model_filepath', '-p', default=None, type=str, help='If present, training will be skipped and the model file + test data from the given directory will be loaded')
    # parser.add_argument('--sleep_wake', '-s', action='store_true', help='If present, the model will be trained to only try to predict sleep vs wake instead of all 4 sleep states')
    return parser.parse_args()


def main():
    args = get_parsed_args()
    print(args)

if __name__ == '__main__':
    main()