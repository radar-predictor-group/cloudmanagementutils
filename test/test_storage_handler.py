import shutil
import unittest
from storage_handler import StorageHandler
from test.test_utils import TestLogger
import os

CLOUD_GENERATED_BASEPATH = 'unittest_files/'
PERMANENT_TESTFILE_NAME = 'file_exists_testfile.txt'
CLOUD_RELATIVE_BASEPATH = 'files'
A1 = os.path.join(CLOUD_RELATIVE_BASEPATH, 'a1.txt')
TEST_IMG = os.path.join(CLOUD_RELATIVE_BASEPATH, 'test.png')
LOCAL_GENERATED_BASEPATH = 'generated'
LOCAL_BASEPATH = 'test'
LOCAL_A1 = os.path.join(LOCAL_BASEPATH, A1)
LOCAL_TEST_IMG = os.path.join(LOCAL_BASEPATH, TEST_IMG)
LOCAL_GENERATED_A1 = os.path.join(LOCAL_GENERATED_BASEPATH, A1)
LOCAL_GENERATED_TEST_IMG = os.path.join(LOCAL_GENERATED_BASEPATH, TEST_IMG)

class TestStorageHandler(unittest.TestCase):

    def setUp(self):
        self._test_logger = TestLogger()
        self._storage_handler = StorageHandler(CLOUD_GENERATED_BASEPATH, self._test_logger)

    def tearDown(self):
        with StorageHandler(CLOUD_GENERATED_BASEPATH, self._test_logger) as storage_handler:
            storage_handler.delete_dir()
        if os.path.isdir(LOCAL_GENERATED_BASEPATH):
            shutil.rmtree(LOCAL_GENERATED_BASEPATH)

    def test_exists(self):
        with StorageHandler('', self._test_logger) as storage_handler:
            self.assertTrue(storage_handler.exists(PERMANENT_TESTFILE_NAME))

    def test_upload(self):
        self.__upload(A1)
        self.assertTrue(self._storage_handler.exists(A1))

    def test_simple_download(self):
        self.__upload(A1)
        self._storage_handler.download(A1, LOCAL_GENERATED_BASEPATH)
        self.assertTrue(os.path.isfile(LOCAL_GENERATED_A1))

    def test_delete(self):
        self.__upload(A1)
        self._storage_handler.delete(A1)
        self.assertFalse(self._storage_handler.exists(A1))

    def test_count_1(self):
        self.__upload(A1)
        self.assertEqual(1, self._storage_handler.count())
        self._storage_handler.delete(A1)
        self.assertEqual(0, self._storage_handler.count())

    def test_get_content_type(self):
        self.assertEquals('text/plain', self._storage_handler._StorageHandler__get_content_type(LOCAL_A1))
        self.assertEquals('image/png', self._storage_handler._StorageHandler__get_content_type(LOCAL_TEST_IMG))


    def test_delete_dir(self):
        test_cloud_filename = A1
        self._storage_handler.upload(LOCAL_A1, test_cloud_filename)
        self.assertTrue(self._storage_handler.exists(test_cloud_filename))
        # TODO get current count of files then assert it only goes down by 1 after this operation
        with StorageHandler('', self._test_logger) as storage_handler:
            storage_handler.delete_dir(CLOUD_GENERATED_BASEPATH)
        self.assertFalse(self._storage_handler.exists(test_cloud_filename))



# tests to add:
# 1) directory structure in GCP matches local directory structure. Do this by uploading all the files
    # in the path test/files to GCP test base path, then download the whole structure to test/tmp/files
    # and compare the 2 directories

    def __upload(self, filename):
        test_cloud_filename = filename
        local_filename = os.path.join(LOCAL_BASEPATH, filename)
        self._storage_handler.upload(local_filename, test_cloud_filename)


if __name__ == '__main__':
    unittest.main()
